#include<iostream>
#include<fstream>
#include<vector>
#include<queue>
#include<cstring>
#include<stdlib.h>
using namespace std;

int M[100][100][100]={0};



class AutomatFinitDeterminist;

struct Acceptor
{
	vector <char> CaractereAcceptate;
	friend class AutomatFinitDeterminist;
};

int nr_stari;
string word;
Acceptor Automat[100][100];

class AutomatFinitDeterminist{
public:

 int VectorDeStariF[5]={0};
    int NumarStariFinale;
    int **T;
    int NumarNoduriSuplimentare;
    int DimensiuneAlfabet;
    char VectorAlfabet[100];
    int nr_stari;
    int NumarTranzitii;
    string word;
    vector <int> stari_finale;
    Acceptor Automat[100][100];
    int viz[100]={0};
    int StareInitiala;

    void InitT(){
             T = new int*[15];
                for (int i = 0; i < 15; ++i)
                    T[i] = new int[15];
                for(int i=0;i<15;i++)
                {for(int j=0;j<15;j++)
                {T[i][j]=0;
        }}

}
    void Set_DimensiuneAlfabet(int a){DimensiuneAlfabet=a;}
    void Set_VectorAlfabet(char a[100]){for(int i=0;i<DimensiuneAlfabet;i++)
                                            VectorAlfabet[i]=a[i];}
    void Set_NumarStari(int b){nr_stari=b;}

    void TabelInchideri();
    void citire();
    int TestFinalizare(int);
    int parcurgere();
    void GetWord(){if(word=="$")cout<<"lambda";else cout<<word;}
    int VerificareLambda();
    void Inchidere(int,int,int,int);
    void Actualizare(int*,int);
    void TestInchidere();
    void AfisareTabel();
    int ComparareVector(int*,int**,int,int);
    void Copiere(int*,int**,int,int);
    void CitireSpeciala(AutomatFinitDeterminist&);
    void Auxiliar();
};

void AutomatFinitDeterminist::Auxiliar(){cout<<endl;
for(int i=0;i<=NumarNoduriSuplimentare;i++)
        for(int o=0;o<nr_stari;o++)
                        cout<<T[i][o];
            cout<<endl;}
int AutomatFinitDeterminist::ComparareVector(int *v,int **w,int dim,int indice){

int k=0;
for(int i=0;i<dim;i++)
    if(v[i]==w[indice][i])
    k++;

if(k==dim)
    return 1;
else return 0;


}
void AutomatFinitDeterminist::Copiere(int*v,int**w,int dim,int indice){


for(int i=0;i<dim;i++)
    w[indice][i]=v[i];

}
void AutomatFinitDeterminist::Actualizare(int *v,int dim)
{for(int i=0;i<dim;i++)
    v[i]=0;
}

void AutomatFinitDeterminist::Inchidere(int nod,int aux,int Pas,int litera)
{
    //cout<<"Parcurgem in adancime nodul : "<<nod<<endl;
    viz[nod]=1;
    if(nod==aux && Pas==0)
        viz[nod]=0;
    for(int k=0;k<nr_stari;k++)
    for (int j = 0; j < Automat[nod][k].CaractereAcceptate.size(); j++)
        if( (Automat[nod][k].CaractereAcceptate[j] == VectorAlfabet[DimensiuneAlfabet]
             || Automat[nod][k].CaractereAcceptate[j] == VectorAlfabet[litera]) && viz[k]==0)
            Inchidere(k,aux,Pas+1,litera);

}
void AutomatFinitDeterminist::TestInchidere(){

int litera;
cout<<endl;
int Pas=0;
litera=0;
Inchidere(1,1,Pas,litera);
 for(int i=0;i<nr_stari;i++)
        {cout<<viz[i];
        if(viz[i]==1)
        M[litera][0][i]=1;
        cout<<endl;}
     for(int i=0;i<nr_stari;i++)
        cout<<M[litera][0][i];
        cout<<endl;
}
void AutomatFinitDeterminist::TabelInchideri(){
    int K=0;
    int Q;
InitT();
for(int i=0;i<nr_stari;i++)
    if(i==StareInitiala)
    T[0][i]=1;

    cout<<endl;
cout<<"Aici incepe T ";
for(int i=0;i<nr_stari;i++)
    cout<<T[0][i];
    cout<<endl;
int AUX[100]={0};
cout<<"Aici incepe AUX ";
for(int i=0;i<nr_stari;i++)
    cout<<AUX[i];
    cout<<endl;
int ind=1;
int nod=0;

int Pas,i,j;
while(ind!=0)
{   int litera=0;
    ind=0;
    while(litera<DimensiuneAlfabet)
    {
        for(i=0;i<nr_stari;i++)
        {   Pas=0;
            if(T[nod][i]==1)
                Inchidere(i,i,Pas,litera);
                for(j=0;j<nr_stari;j++)
                    if(viz[j]==1)
                    M[litera][nod][j]=1;
                cout<<"Aici incepe VIZ ";
                for(int o=0;o<nr_stari;o++)
                    cout<<viz[o];
                    cout<<endl;
            Actualizare(viz,nr_stari);
            cout<<"Aici incepe VIZ ";
                for(int o=0;o<nr_stari;o++)
                    cout<<viz[o];
                    cout<<endl;
            cout<<"Aici incepe M ";
                for(int o=0;o<nr_stari;o++)
                    cout<<M[litera][nod][o];
                    cout<<endl;
        }
        for(i=0;i<nr_stari;i++)
            AUX[i]=M[litera][nod][i];
            cout<<"Aici incepe AUX ";
                for(int o=0;o<nr_stari;o++)
                    cout<<AUX[o];
                    cout<<endl;
            cout<<"Aici incepe M special ";
                for(int o=0;o<nr_stari;o++)
                    cout<<M[litera][nod][o];
                    cout<<endl;
            Q=0;
            for(i=0;i<=K;i++)
                if(ComparareVector(AUX,T,nr_stari,i)==1)
                    Q++;
                    cout<<"Aici incepe Q ";
                    cout<<Q;
                    cout<<endl;
                    if(Q==0)
                   {
                       ind++;
                        K++;
                   Copiere(AUX,T,nr_stari,K);
                      cout<<"Aici incepe T dupa copiere ";
                for(int o=0;o<nr_stari;o++)
                    cout<<T[K][o];
                    cout<<endl;
                   Actualizare(AUX,nr_stari);
                      cout<<"Aici incepe AUX dupa actualizare ";
                for(int o=0;o<nr_stari;o++)
                    cout<<AUX[o];
                    cout<<endl;
                   }
                   litera++;
    }nod++;
    if(ind==0 && nod==K)
        ind++;
}



for(int a=0;a<DimensiuneAlfabet;a++)
    {
        cout<<endl;
        cout<<VectorAlfabet[a]<<endl;
    for(int b=0;b<=K;b++)
        {
            cout<<endl;
            cout<<"Nodul: "<<b<<endl;
            cout<<endl;
        for(int c=0;c<nr_stari;c++)
            cout<<M[a][b][c]<<" ";
            }

    }

    NumarNoduriSuplimentare=K;

    system("cls");
for(int a=0;a<DimensiuneAlfabet;a++)
{int k=0;
    while(k<=NumarNoduriSuplimentare){
    cout<<endl;
    cout<<"q(";
int x=0;
int y=0;
    for(int o=0;o<nr_stari;o++)
        if(T[k][o]==1)
        {cout<<o;
        x++;
        for(int j=0;j<NumarStariFinale;j++)
            if(o==stari_finale[j])
                 cout<<") <== Stare Finala";
        }
        if(x==0)
            cout<<"NULL";
            cout<<")  =  q(";
         for(int c=0;c<nr_stari;c++)
            if(M[a][k][c]==1)
        {cout<<c;
        y++;}
        if(y==0)
            cout<<"NULL";
         cout<<", "<<VectorAlfabet[a];
         cout<<")";


         if(k==StareInitiala)
         cout<<" <== Stare Initiala";
         cout<<endl;
         k++;
}
}
cout<<endl;
int k=0;
  while(k<=NumarNoduriSuplimentare){
    {cout<<"Nodul "<<k<<" arata de forma: ";
    cout<<"q(";
    int x=0;
    for(int o=0;o<nr_stari;o++)
        if(T[k][o]==1)
        {x++;cout<<o;}
        if(x==0)
        cout<<"NULL";
        cout<<")"<<endl;}


k++;}
cout<<endl;}

void AutomatFinitDeterminist::CitireSpeciala(AutomatFinitDeterminist &B){


    DimensiuneAlfabet=B.DimensiuneAlfabet;

    for(int i=0;i<B.DimensiuneAlfabet;i++){
        VectorAlfabet[i]=B.VectorAlfabet[i];
       }
    nr_stari=B.nr_stari;

    int StariAux=nr_stari;
    nr_stari=B.NumarNoduriSuplimentare;

	 word=B.word;

	int i, x,nr_tranzitii,k,j,n;
		StareInitiala=B.StareInitiala;

		NumarStariFinale=B.NumarStariFinale;

		NumarStariFinale=B.stari_finale.size();
		for(int i=0;i<NumarStariFinale;i++)

	char litera;
	//nr_tranzitii=NumarNoduriSuplimentare;
	//NumarTranzitii=nr_tranzitii;
	int AUX[100]={0};
	NumarTranzitii=0;

	for(int a=0;a<DimensiuneAlfabet;a++)
    {


    for(int b=0;b<=nr_stari;b++)
        {
        for(int c=0;c<StariAux;c++)
            AUX[c]=M[a][b][c];


            for(int i=0;i<=StariAux;i++)
                if(ComparareVector(AUX,B.T,StariAux,i)==1)
                   {Automat[b][i].CaractereAcceptate.push_back(VectorAlfabet[a]);
                   NumarTranzitii++;}

            }

    }


for(int i=0;i<NumarStariFinale;i++)
		 VectorDeStariF[i]=B.stari_finale[i];


	for (i = 0; i <= nr_stari; i++)
	{
		for (j = 0; j <= nr_stari; j++)
		{
			if (Automat[i][j].CaractereAcceptate.size() == 0)
				cout << '0';
			for (k = 0; k < Automat[i][j].CaractereAcceptate.size(); k++)
				cout << Automat[i][j].CaractereAcceptate[k];
			cout << " ";
		}
		cout << endl;
	}
     int u=0;


	for(int o=0;o<=nr_stari;o++)
	 {
	     for(int j=0;j<StariAux;j++)
        {
            if(B.T[o][j]==1)
            {
                for(int b=0;b<NumarStariFinale;b++)
               {
                   if(j==B.stari_finale[b])
                {
                    VectorDeStariF[u]=o;
                    u++;

                }
               }
            }
        }
    }


                NumarStariFinale=u;



}

void AutomatFinitDeterminist::citire()
{
	ifstream f("Intrare.in");
    f>>DimensiuneAlfabet;

    DimensiuneAlfabet=DimensiuneAlfabet+1;

    f>>VectorAlfabet;
    int o=strlen(VectorAlfabet);
    VectorAlfabet[o]='$';
    DimensiuneAlfabet=o;
	f >> nr_stari;
	word=" ";
	cout<<"Inserati cuvantul de verificat "<<endl;
	cin >> word;
	int i, x,nr_tranzitii,k,j,n;
		f >> x;
		StareInitiala=x-1;
		f >> NumarStariFinale;
		for(i=0;i<NumarStariFinale;i++)
		{f >> x;
            stari_finale.push_back(x-1);}
	char litera;
	f >> nr_tranzitii;
	NumarTranzitii=nr_tranzitii;
	while (nr_tranzitii != 0)
	{
		f >> i >> j >> litera;
		Automat[i-1][j-1].CaractereAcceptate.push_back(litera);
		nr_tranzitii--;
	}

	for (i = 0; i < nr_stari; i++)
	{
		for (j = 0; j < nr_stari; j++)
		{
			if (Automat[i][j].CaractereAcceptate.size() == 0)
				cout << '0';
			for (k = 0; k < Automat[i][j].CaractereAcceptate.size(); k++)
				cout << Automat[i][j].CaractereAcceptate[k];
			cout << " ";
		}
		cout << endl;
	}
}

int AutomatFinitDeterminist::TestFinalizare(int x)
{
	int i;
	for (i = 0; i < NumarStariFinale; i++)
		if (x == VectorDeStariF[i])
			return 1;
	return 0;
}

int AutomatFinitDeterminist::VerificareLambda(){
string lambda=("$");
for(int i=0; i< NumarStariFinale; i++)
if(word.compare(lambda)==0 && StareInitiala==VectorDeStariF[i])
        return 1;
return 0;
}

int AutomatFinitDeterminist::parcurgere()
{
    cout<<endl;


    nr_stari=nr_stari+1;
    StareInitiala=0;

	queue <pair <char*,int> > coada;
	int i, j;
	if(VerificareLambda()==1)
        return 1;
    if(word.size()==1)
        for (i = 0; i < nr_stari; i++)
            for (j = 0; j < Automat[StareInitiala][i].CaractereAcceptate.size(); j++)
			    if (Automat[StareInitiala][i].CaractereAcceptate[j] == word[0])
                for(int p=0; p< NumarStariFinale; p++)
                    if(i==VectorDeStariF[p])
                        return 1;

	for (i = 0; i < nr_stari; i++)
		for (j = 0; j < Automat[StareInitiala][i].CaractereAcceptate.size(); j++)
			if (Automat[StareInitiala][i].CaractereAcceptate[j] == word[0])
			{
				const char* WordAux=word.c_str();

				pair <char*,int> a;
				a.first = new char[100];
				strcpy(a.first, WordAux + 1);
				a.second = i;
				coada.push(a);
			}
	while (!coada.empty())
	{
		pair<char*, int> a;
		a.first = new char[100];
		strcpy(a.first,coada.front().first);
		a.second = coada.front().second;
		coada.pop();
		for (int i = 0; i < nr_stari; i++)
			for (int j = 0; j < Automat[a.second][i].CaractereAcceptate.size(); j++)
				if (Automat[a.second][i].CaractereAcceptate[j] == a.first[0])
					if (strlen(a.first) == 1 && TestFinalizare(i))
						return 1;
					else
					{
						pair< char*, int> b;
						b.first = new char[100];
						strcpy(b.first, a.first + 1);
						b.second = i;
						coada.push(b);
					}
	}
	return 0;
}


void Program(){
    AutomatFinitDeterminist A;
    AutomatFinitDeterminist B;


A.citire();


cout<<endl;
A.TabelInchideri();

B.CitireSpeciala(A);




	if (B.parcurgere() == 1)
		{cout << "Cuvantul ";B.GetWord();cout<<" e acceptat de automat!";}
	else

		{cout << "Cuvantul ";B.GetWord();cout<<" nu este acceptat de automat!";}
}

int main()
{
	Program();
}
